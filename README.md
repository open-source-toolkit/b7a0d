# Axure RP Extension for Chrome for Mac 资源下载

## 简介
本仓库提供了一个专为Mac用户设计的Axure RP Extension for Chrome插件资源文件。该插件允许用户在谷歌浏览器中直接查看和交互Axure原型，极大地提高了设计和开发过程中的效率和便捷性。

## 资源描述
- **标题**: Axure RP Extension for Chrome for Mac
- **描述**: Axure RP Extension for ChromeMac Axure RP ExtensionAxure RP Extension

## 下载与安装
1. 点击仓库中的资源文件进行下载。
2. 下载完成后，解压文件。
3. 打开谷歌浏览器，进入扩展程序管理页面（chrome://extensions/）。
4. 开启“开发者模式”。
5. 点击“加载已解压的扩展程序”，选择解压后的文件夹进行安装。

## 使用说明
安装完成后，您可以在谷歌浏览器中直接打开Axure原型文件，无需启动Axure软件即可查看和交互原型。

## 注意事项
- 请确保您的谷歌浏览器版本支持该插件。
- 如有任何安装或使用问题，请参考官方文档或联系技术支持。

## 贡献
欢迎提交问题和改进建议，帮助我们不断完善这个资源文件。

## 许可证
本资源文件遵循相应的开源许可证，具体信息请查看LICENSE文件。

---
感谢您的使用和支持！